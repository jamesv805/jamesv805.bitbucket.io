var searchData=
[
  ['cal_5fcoeff_0',['cal_coeff',['../classi2c_1_1_b_n_o055.html#a96cc79b8c508bfe1d3e3f302e4f9da3e',1,'i2c::BNO055']]],
  ['cal_5fstatus_1',['cal_status',['../classi2c_1_1_b_n_o055.html#abf606d9d88ef9fb202fb26fc1b325530',1,'i2c::BNO055']]],
  ['check_2',['check',['../namespace_lab__0x01___baechler_verheyden.html#a299a50c16ca44feb5c1acf17a0273729',1,'Lab_0x01_BaechlerVerheyden']]],
  ['closedloop_3',['closedloop',['../namespaceclosedloop.html',1,'']]],
  ['closedloop_4',['ClosedLoop',['../classtask__user_1_1_task___user.html#a3275a8677dd94f546c171c12e06cf4f5',1,'task_user.Task_User.ClosedLoop()'],['../classclosedloop_1_1_closed_loop.html',1,'closedloop.ClosedLoop']]],
  ['closedloop_2epy_5',['closedloop.py',['../closedloop_8py.html',1,'']]],
  ['controls_6',['controls',['../classtask__user_1_1_task___user.html#a903f8a855b42b203116d21579b76ca01',1,'task_user::Task_User']]],
  ['count_7',['count',['../classencoder__updated_1_1_encoder.html#ac4e02e6d23386821e3ac908216c6872f',1,'encoder_updated.Encoder.count()'],['../classtask__user_1_1_task___user.html#a0e19164fc7bcdd059bdfcb5b18827374',1,'task_user.Task_User.count()']]],
  ['current2_8',['current2',['../namespace_lab__0x01___baechler_verheyden.html#a6079fc66b460348151685697c51935ae',1,'Lab_0x01_BaechlerVerheyden']]],
  ['current3_9',['current3',['../namespace_lab__0x01___baechler_verheyden.html#a74395690cd171946cf9277dd8b337afe',1,'Lab_0x01_BaechlerVerheyden']]],
  ['current4_10',['current4',['../namespace_lab__0x01___baechler_verheyden.html#aee9f1d16ccff169568f6de778d5010b3',1,'Lab_0x01_BaechlerVerheyden']]],
  ['current_5ftime_11',['current_time',['../namespace_lab__0x01___baechler_verheyden.html#ac5b24110751106bbea31ea72d8639fb9',1,'Lab_0x01_BaechlerVerheyden']]],
  ['current_5ftime2_12',['current_time2',['../namespace_lab__0x01___baechler_verheyden.html#a4583a190beec816f41ccc5aa4a5f9f03',1,'Lab_0x01_BaechlerVerheyden']]],
  ['current_5ftime3_13',['current_time3',['../namespace_lab__0x01___baechler_verheyden.html#ae766930ca9c685c37d308cea1eb9606b',1,'Lab_0x01_BaechlerVerheyden']]],
  ['current_5ftime4_14',['current_time4',['../namespace_lab__0x01___baechler_verheyden.html#a8d5b69ee0bfb760b23260894e90baa61',1,'Lab_0x01_BaechlerVerheyden']]]
];
