var namespace_i_m_u__driver =
[
    [ "BNO055", "class_i_m_u__driver_1_1_b_n_o055.html", "class_i_m_u__driver_1_1_b_n_o055" ],
    [ "filename", "namespace_i_m_u__driver.html#af230785e4f780f69835bea75ced1bc1d", null ],
    [ "i2c", "namespace_i_m_u__driver.html#a114e22796b92c5dd0e6240e6339fd671", null ],
    [ "pin_xm", "namespace_i_m_u__driver.html#af0c3f1b4bd34aaaa7a68289391ffa33f", null ],
    [ "pin_xp", "namespace_i_m_u__driver.html#af52cda6bd2f619d84286f9e27abf963a", null ],
    [ "pin_ym", "namespace_i_m_u__driver.html#aad688ccc0375a5e303cdb220a784ca99", null ],
    [ "pin_yp", "namespace_i_m_u__driver.html#a57e3cb45c875a324461f54ac647b7f3f", null ],
    [ "sensor", "namespace_i_m_u__driver.html#abcc6cbec6e5b9890af369772d6883324", null ],
    [ "TP", "namespace_i_m_u__driver.html#ad1e1004394e1090292d6c8d65d6f2c61", null ]
];