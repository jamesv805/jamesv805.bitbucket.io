var annotated_dup =
[
    [ "closedloop", "namespaceclosedloop.html", [
      [ "ClosedLoop", "classclosedloop_1_1_closed_loop.html", "classclosedloop_1_1_closed_loop" ]
    ] ],
    [ "datacollection", "namespacedatacollection.html", [
      [ "DataCollection", "classdatacollection_1_1_data_collection.html", "classdatacollection_1_1_data_collection" ]
    ] ],
    [ "encoder_updated", "namespaceencoder__updated.html", [
      [ "Encoder", "classencoder__updated_1_1_encoder.html", "classencoder__updated_1_1_encoder" ]
    ] ],
    [ "i2c", "namespacei2c.html", [
      [ "BNO055", "classi2c_1_1_b_n_o055.html", "classi2c_1_1_b_n_o055" ]
    ] ],
    [ "IMU_driver", "namespace_i_m_u__driver.html", [
      [ "BNO055", "class_i_m_u__driver_1_1_b_n_o055.html", "class_i_m_u__driver_1_1_b_n_o055" ]
    ] ],
    [ "IMU_task", "namespace_i_m_u__task.html", [
      [ "IMU_Task", "class_i_m_u__task_1_1_i_m_u___task.html", "class_i_m_u__task_1_1_i_m_u___task" ]
    ] ],
    [ "motor_driver", "namespacemotor__driver.html", [
      [ "DRV8847", "classmotor__driver_1_1_d_r_v8847.html", "classmotor__driver_1_1_d_r_v8847" ],
      [ "Motor", "classmotor__driver_1_1_motor.html", "classmotor__driver_1_1_motor" ]
    ] ],
    [ "task_encoder", "namespacetask__encoder.html", [
      [ "Task_Encoder", "classtask__encoder_1_1_task___encoder.html", "classtask__encoder_1_1_task___encoder" ]
    ] ],
    [ "task_user", "namespacetask__user.html", [
      [ "Task_User", "classtask__user_1_1_task___user.html", "classtask__user_1_1_task___user" ]
    ] ],
    [ "touchpanel_driver", "namespacetouchpanel__driver.html", [
      [ "TouchPanel", "classtouchpanel__driver_1_1_touch_panel.html", "classtouchpanel__driver_1_1_touch_panel" ]
    ] ],
    [ "UI_task", "namespace_u_i__task.html", [
      [ "UI_Task", "class_u_i__task_1_1_u_i___task.html", "class_u_i__task_1_1_u_i___task" ]
    ] ]
];