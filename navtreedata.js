/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "James Verheyden ME305", "index.html", [
    [ "Lab Overview", "index.html", [
      [ "Introduction", "index.html#part1main", null ],
      [ "Labs", "index.html#part2main", null ]
    ] ],
    [ "Lab 0", "lab0.html", [
      [ "Lab 0 Fibonacci", "lab0.html#part1lab0", null ]
    ] ],
    [ "Lab 1", "lab1.html", [
      [ "Lab 1 LED Driver", "lab1.html#part1lab1", null ]
    ] ],
    [ "Lab 2", "lab2.html", [
      [ "Lab 2 Incremental Encoders and User Interface", "lab2.html#part1lab2", null ]
    ] ],
    [ "Lab 3", "lab3.html", [
      [ "Lab 3 PMDC Motors", "lab3.html#part1lab3", null ],
      [ "Deliverables", "lab3.html#part2lab3", null ]
    ] ],
    [ "Lab 4", "lab4.html", [
      [ "Lab 4 Closed Loop Controller", "lab4.html#part1lab4", null ],
      [ "Deliverables", "lab4.html#part2lab4", null ]
    ] ],
    [ "Lab 5", "lab5.html", [
      [ "Lab 5 Communication and IMU", "lab5.html#part1lab5", null ]
    ] ],
    [ "Final", "lab6.html", [
      [ "Final Project", "lab6.html#part1lab6", null ],
      [ "Touch Panel Testing", "lab6.html#part2lab6", null ],
      [ "Gain Calculations", "lab6.html#part3lab6", null ]
    ] ],
    [ "HW 2", "hw2.html", [
      [ "Calculations", "hw2.html#part1hw2", null ]
    ] ],
    [ "HW 3", "hw3.html", [
      [ "Model and Calculations", "hw3.html#part1hw3", null ]
    ] ],
    [ "Namespaces", "namespaces.html", [
      [ "Namespace List", "namespaces.html", "namespaces_dup" ],
      [ "Namespace Members", "namespacemembers.html", [
        [ "All", "namespacemembers.html", null ],
        [ "Functions", "namespacemembers_func.html", null ],
        [ "Variables", "namespacemembers_vars.html", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"_i_m_u__driver_8py.html",
"classtask__encoder_1_1_task___encoder.html#ab2b528811638fee71501f72e2e22ef90",
"namespace_u_i__task.html#a1c335ebd99e5d242ff5b2496ff914120"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';