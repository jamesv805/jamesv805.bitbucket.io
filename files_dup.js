var files_dup =
[
    [ "docpages", "dir_a86259847659d41e31fc517b099e4d91.html", null ],
    [ "closedloop.py", "closedloop_8py.html", [
      [ "closedloop.ClosedLoop", "classclosedloop_1_1_closed_loop.html", "classclosedloop_1_1_closed_loop" ]
    ] ],
    [ "datacollection.py", "datacollection_8py.html", "datacollection_8py" ],
    [ "encoder_updated.py", "encoder__updated_8py.html", [
      [ "encoder_updated.Encoder", "classencoder__updated_1_1_encoder.html", "classencoder__updated_1_1_encoder" ]
    ] ],
    [ "finalmain.py", "finalmain_8py.html", "finalmain_8py" ],
    [ "i2c.py", "i2c_8py.html", "i2c_8py" ],
    [ "IMU_driver.py", "_i_m_u__driver_8py.html", "_i_m_u__driver_8py" ],
    [ "IMU_task.py", "_i_m_u__task_8py.html", "_i_m_u__task_8py" ],
    [ "Lab_0x01_BaechlerVerheyden.py", "_lab__0x01___baechler_verheyden_8py.html", "_lab__0x01___baechler_verheyden_8py" ],
    [ "main.py", "main_8py.html", "main_8py" ],
    [ "mainpage.py", "mainpage_8py.html", null ],
    [ "ME305Lab0.py", "_m_e305_lab0_8py.html", "_m_e305_lab0_8py" ],
    [ "motor_driver.py", "motor__driver_8py.html", "motor__driver_8py" ],
    [ "task_encoder.py", "task__encoder_8py.html", [
      [ "task_encoder.Task_Encoder", "classtask__encoder_1_1_task___encoder.html", "classtask__encoder_1_1_task___encoder" ]
    ] ],
    [ "task_user.py", "task__user_8py.html", "task__user_8py" ],
    [ "touchpanel_driver.py", "touchpanel__driver_8py.html", "touchpanel__driver_8py" ],
    [ "UI_task.py", "_u_i__task_8py.html", "_u_i__task_8py" ]
];