var classclosedloop_1_1_closed_loop =
[
    [ "__init__", "classclosedloop_1_1_closed_loop.html#a5353785a17f2d4e84e12f7130c4bc191", null ],
    [ "get_Kp", "classclosedloop_1_1_closed_loop.html#aabfd126eb373a40747f7fd312ed0056c", null ],
    [ "run", "classclosedloop_1_1_closed_loop.html#ab160460cb1732e84a9d11c0589f5b4a4", null ],
    [ "set_Kp", "classclosedloop_1_1_closed_loop.html#a617a88880b37c7434947936e1d3a37ce", null ],
    [ "Kp", "classclosedloop_1_1_closed_loop.html#aae8c3c1b5cb36912bca6b14089e108f3", null ],
    [ "Kt", "classclosedloop_1_1_closed_loop.html#a89faa01abb65be7a1f47f3244ec8e6b1", null ],
    [ "next_time", "classclosedloop_1_1_closed_loop.html#a13d99b5cfc5e54c0ae66c96b4508fada", null ],
    [ "period", "classclosedloop_1_1_closed_loop.html#a8a2b627ea1328f35b4e0f785c267d1b1", null ],
    [ "R", "classclosedloop_1_1_closed_loop.html#ae7169823992ae584d1f9a1551acadab9", null ],
    [ "utime", "classclosedloop_1_1_closed_loop.html#adc4d54cad39fa7b9ad7dd4dd252c9a8d", null ],
    [ "Vdc", "classclosedloop_1_1_closed_loop.html#ae5e681f1ca861032a527f7621848385d", null ]
];