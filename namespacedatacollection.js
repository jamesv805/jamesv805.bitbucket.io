var namespacedatacollection =
[
    [ "DataCollection", "classdatacollection_1_1_data_collection.html", "classdatacollection_1_1_data_collection" ],
    [ "data", "namespacedatacollection.html#a046439e499232c08ae19da47a524c5ed", null ],
    [ "i2c", "namespacedatacollection.html#a7ee5eee8a2ee2cb7e54ddf9110ce9027", null ],
    [ "pin_xm", "namespacedatacollection.html#ac0ebcc6523bb9d52e2c20c727e2b7816", null ],
    [ "pin_xp", "namespacedatacollection.html#acedf85ea7d7c8e82b88c96c97de62af3", null ],
    [ "pin_ym", "namespacedatacollection.html#a89a8779bfe1716c877199aa36ae9d696", null ],
    [ "pin_yp", "namespacedatacollection.html#a6c4ca781f239dc8c2f74882521708269", null ],
    [ "sensor", "namespacedatacollection.html#a748dc3c095885e5a2827656e7b2d6720", null ],
    [ "TP", "namespacedatacollection.html#a2134d3082dd7fc497a5e6650113fea11", null ]
];